# https://gitlab.khronos.org/Tracker/vk-gl-cts/-/issues/3505
dEQP-VK.subgroups.multiple_dispatches.uniform_subgroup_size,Fail

# Fails when TU_DEBUG=forcebin is set
gmem-dEQP-VK.spirv_assembly.instruction.graphics.variable_pointers.graphics.writes_two_buffers_geom,Fail
gmem-dEQP-VK.spirv_assembly.instruction.graphics.variable_pointers.graphics.writes_two_buffers_vert,Fail
